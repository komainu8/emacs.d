;;; Copyright (C) 2024  Horimoto Yasuhiro <horimoto@clear-code.com>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;; MELPA
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)
;; 初回起動時はパッケージリストを更新する。
(unless package-archive-contents
  (package-refresh-contents))

;; use-package
(dolist (package '(use-package))
  (unless (package-installed-p package)
    (package-install package)))

;; Mozc
(add-to-list 'load-path "/usr/share/emacs/site-lisp/emacs-mozc")
(require 'mozc)
(setq default-input-method "japanese-mozc")
(define-key global-map (kbd "C-j") 'toggle-input-method)

;; Keybind
(define-key global-map (kbd "C-z") 'undo) ; undo
(define-key global-map (kbd "M-C-g") 'grep) ; grep

;; Packages
(require 'tree-sitter)
(require 'tree-sitter-langs)

(use-package tree-sitter
  :ensure t
  :config
  (global-tree-sitter-mode))

(use-package eglot
  :ensure t
  :config
  (add-hook 'ruby-mode-hook #'eglot-ensure))

(use-package company
  :ensure t
  :bind (("M-<tab>" . company-complete) ;;Tabで自動補完
	 :map company-active-map
	 ("M-n" . nil)
	 ("M-p" . nil)
	 ("C-n" . company-select-next)
	 ("C-p" . company-select-previous)
	 ("C-s" . company-filter-candidates)
	 :map company-search-map
	 ("C-n" . company-select-next)
	 ("C-p" . company-select-previous))
  :init
  (global-company-mode)
  :config
  (setq completion-ignore-case t)
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 2)
  (setq company-selection-wrap-around t)
  (setq company-transformers '(company-sort-by-occurrence company-sort-by-backend-importance)))

(use-package swiper
  :ensure t
  :config
  (defun isearch-forward-or-swiper (use-swiper)
    (interactive "p")
    (let (current-prefix-arg)
      (call-interactively (if use-swiper 'swiper 'isearch-forward))))
  (global-set-key (kbd "C-s") 'isearch-forward-or-swiper)
  )

(use-package ivy
  :ensure t
  )

(use-package counsel
  :ensure t
  :bind (("M-x" . counsel-M-x)
	 ("C-x C-f" . counsel-find-file)) ;; find-fileもcounselで実行
  )

(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)))

(use-package git-gutter
    :ensure t
    :custom
    (git-gutter:modified-sign "~")
    (git-gutter:added-sign    "+")
    (git-gutter:deleted-sign  "-")
    :custom-face
    (git-gutter:modified ((t (:background "#f1fa8c"))))
    (git-gutter:added    ((t (:background "#50fa7b"))))
    (git-gutter:deleted  ((t (:background "#ff79c6"))))
    :config
    (global-git-gutter-mode +1))

(use-package rainbow-delimiters
  :ensure t
  :hook
  (prog-mode . rainbow-delimiters-mode))

(use-package symbol-overlay
  :ensure t
  :config
  (global-set-key (kbd "M-i") 'symbol-overlay-put)
  (global-set-key (kbd "M-q") 'symbol-overlay-remove-all))

(use-package paren
  :ensure nil
  :hook
  (after-init . show-paren-mode)
  :custom-face
  (show-paren-match ((nil (:background "#44475a" :foreground "#f1fa8c"))))
  :custom
  (show-paren-style 'mixed)
  (show-paren-when-point-inside-paren t)
  (show-paren-when-point-in-periphery t))

;;; Major mode
(use-package markdown-mode
  :ensure t
  :mode (("\\.markdown\\'" . markdown-mode)
	 ("\\.md\\'" . markdown-mode)
	 ("README\\.md\\'" . gfm-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package yaml-mode
  :ensure t
  :mode (("\\.yml\\'" . yaml-mode)
	 ("\\.yaml\\'" . yaml-mode)))

;; Misc
;;; 起動時設定
;; 起動画面を表示しない。
(setq inhibit-startup-screen t)

;;; Backup
;; バックアップファイルを作らない
(setq backup-inhibited t)
;; 終了時にオートセーブファイルを消す
(setq delete-auto-save-files t)

;;; Bar
;; メニューバーを消す
(menu-bar-mode -1)
;; ツールバーを消す
(tool-bar-mode -1)

;;; Cursor
;; カーソルの点滅を止める
(blink-cursor-mode 0)

;;; Position
;; 現在行を目立たせる
(global-hl-line-mode)
;; カーソルの位置が何文字目かを表示する
(column-number-mode t)
;; カーソルの位置が何行目かを表示する
(line-number-mode t)

;;; Lines
;; 行の先頭でC-kを一回押すだけで行全体を消去する
(setq kill-whole-line t)
;; 最終行に必ず一行挿入する
(setq require-final-newline t)
;; バッファの最後でnewlineで新規行を追加するのを禁止する
(setq next-line-add-newlines nil)

;;; History
;; 履歴数を大きくする
(setq history-length 1000)
;; ミニバッファの履歴を保存する
(savehist-mode 1)

;;; Compress
;; gzファイルも編集できるようにする
(auto-compression-mode t)

;;; Shebang
;; shebangがあるファイルを保存すると実行権をつける。
(add-hook 'after-save-hook
          'executable-make-buffer-file-executable-if-script-p)

;;; Upcase/Downcase
;; リージョンの大文字小文字変換を有効にする。
;; C-x C-u -> upcase
;; C-x C-l -> downcase
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;;; Kill
;; killしたらprimary selectionにもクリップボードにも入れる
(setq select-enable-primary t)
(setq select-enable-clipboard t)

;;; Alert
;; ビープ音を無効にする。
(setq ring-bell-function 'ignore)

;;; Theme
(use-package doom-themes
  :ensure t
  :custom
  (doom-themes-enable-italic t)
  (doom-themes-enable-bold t)
  :custom-face
  (doom-modeline-bar ((t (:background "#6272a4"))))
  :config
  (load-theme 'doom-dracula t)
  (doom-themes-org-config))

;;; Modeline
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom
  (doom-modeline-buffer-file-name-style 'truncate-with-project)
  (doom-modeline-icon t)
  (doom-modeline-major-mode-icon nil)
  (doom-modeline-minor-modes nil)
  :hook
  (after-init . doom-modeline-mode)
  :config
  (doom-modeline-def-modeline 'main
    '(bar vcs buffer-info buffer-encoding buffer-position)
    '(input-method lsp major-mode minor-modes checker)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(counsel avy-migemo migemo ivy swiper symbol-overlay yaml-mode magit git-gutter doom-modeline highlight-indent-guides rainbow-delimiters treesit-auto tree-sitter eglot use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
